﻿// ReSharper disable ClassNeverInstantiated.Global

namespace GitLab.NET.ResponseModels
{
    /// <summary> Stores information about a project. </summary>
    public class Project { }
}